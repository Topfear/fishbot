import os

from time import sleep
from time import time
import pyautogui
import keyboard


class Stats():
    _success = 0
    _fail = 0
    _total = 0
    _fail_in_a_row = 0

    @classmethod
    def success(cls):
        cls._success += 1
        cls._total += 1
        cls._fail_in_a_row = 0

    @classmethod
    def fail(cls):
        cls._fail += 1
        cls._total += 1
        cls._fail_in_a_row += 1

    @classmethod
    def result(cls):
        print(f"Успешных попыток: {int(cls._success / cls._total * 100)}%")

        if cls._fail_in_a_row >= 10:
            print("Здесь порыбачить не получается")
            os._exit(0)


def exiting(event=None):
    print("🏴‍☠️ Фишбот завершает работу")
    Stats.result()
    os._exit(0)
keyboard.on_press_key('Esc', exiting)


img_floats = sorted(os.listdir('images'))
def find_float(region=(470, 290, 950, 420), confidence=0.6):
    for img_float in img_floats:
        coords = pyautogui.locateCenterOnScreen(f"images/{img_float}", confidence=confidence, region=region)

        if coords is None:
            continue
        else:
            print(f'Поплавок найден по координатам {coords}')
            return coords

    print("Не могу найти поплавок...")
    return None


def throw_float():
    print("🎣 Бросаю поплавок")  
    pyautogui.press('1')  
    sleep(3)


def catch_fish(coords, faied=False):
    print("🐠 Ловлю рыбу")
    pyautogui.moveTo(coords.x, coords.y, duration=0.2)
    pyautogui.click(button='right')
    if faied:
        Stats.fail()
    else:
        Stats.success()


def track_float(float_coords):
    print("👀 Отслеживаю поплавок")
    start = time()

    prev_float_pos = float_coords
    while True:
        time_now = time()
        if time_now - start > 15:
            print("Похоже поплавок не двигался...")
            Stats.fail()
            break

        float_region = (float_coords.x - 50, float_coords.y - 50, 100, 100)
        float_pos = find_float(region=float_region, confidence=0.30)

        if not float_pos:
            # не нашёл заканчиваем
            catch_fish(float_coords, faied=True)
            break

        float_moves = (abs(float_pos.x - prev_float_pos.x) >= 9) or (abs(float_pos.y - prev_float_pos.y) >= 7)

        if float_moves:
            # поплавок дёрнулся
            catch_fish(float_coords)
            break
        
        prev_float_pos = float_pos
        sleep(0.1)


def main():
    print('🚀 Фишбот запущен') 
    print('Жду 4 секунды чтобы переключиться на WoW') 
    sleep(4)

    while True:
        # закидываем удочку
        throw_float()

        # Ищем поплавок
        float_coords = find_float()

        # поплавок найден -> отслеживаем качание
        # поплавок не найден -> запускаем цикл заново
        if float_coords:
            track_float(float_coords)
        else:
            Stats.fail()

        Stats.result()
        sleep(2)


main()
