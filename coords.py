import os
from time import sleep
import keyboard

def exiting(e):
    print("🏴‍☠️ Фишбот завершает работу")
    os._exit(0)


while True:
    keyboard.on_press_key('Esc', exiting)
    sleep(1)
